# VNGATE - VIETNAM B2B GATEWAY

This is a national project for B2B traders. 
We carefully select and verify the best businesses in a variety of industries in Vietnam in order to introduce and secure trade between reputable sellers in Vietnam and buyers all over the world.